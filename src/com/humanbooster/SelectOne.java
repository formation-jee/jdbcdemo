package com.humanbooster;

import java.sql.*;

public class SelectOne {
    public static void main(String[] args) {
        String url="jdbc:mysql://localhost/jdbccourses";
        String user="root";
        String password="tiger";
        try {
            Connection cnx = DriverManager.getConnection(url,
                    user, password);
            PreparedStatement preparedStatement = cnx.prepareStatement("select * from category where id = ?");
            preparedStatement.setInt(1, 1);
            ResultSet rs = preparedStatement.executeQuery();
            while (rs.next()) {
                int id = rs.getInt("id");
                String libelle = rs.getString("libelle");
                System.out.println(id + "," + libelle);
            }
            cnx.close();
        } catch (SQLException e) {
            System.out.println("Une erreur est survenue lors de la connexion à la base de données");
            e.printStackTrace();
        }
    }
}
