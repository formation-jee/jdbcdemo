package com.humanbooster;

import java.sql.*;

public class Insert {
    public static void main(String[] args) {
        String url="jdbc:mysql://localhost/jdbccourses";
        String user="root";
        String password="tiger";
        try {
            Connection cnx = DriverManager.getConnection(url,
                    user, password);
            PreparedStatement preparedStatement = cnx.prepareStatement("INSERT INTO category (libelle) VALUES (?)");
            preparedStatement.setString(1, "Catégorie ajouté par JSE");
            preparedStatement.execute();
            cnx.close();
        } catch (SQLException e) {
            System.out.println("Une erreur est survenue lors de la connexion à la base de données");
            e.printStackTrace();
        }
    }
}
