package com.humanbooster;

import java.sql.*;

public class SelectAll {

    public static void main(String[] args) {
        String url="jdbc:mysql://localhost/jdbccourses";
        String user="root";
        String password="tiger";
        try {
            Connection cnx = DriverManager.getConnection(url,
                    user, password);

            Statement stmt = cnx.createStatement();
            ResultSet rs = stmt.executeQuery("SELECT * FROM category");
            while (rs.next()) {
                int id = rs.getInt("id");
                String libelle = rs.getString("libelle");
                System.out.println(id + "," + libelle);
            }
            cnx.close();
        } catch (SQLException e) {
            System.out.println("Une erreur est survenue lors de la connexion à la base de données");
            e.printStackTrace();
        }
    }
}
