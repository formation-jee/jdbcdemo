package com.humanbooster;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;

public class Update {
    public static void main(String[] args) {
        String url="jdbc:mysql://localhost/jdbccourses";
        String user="root";
        String password="tiger";
        try {
            Connection cnx = DriverManager.getConnection(url,
                    user, password);
            PreparedStatement preparedStatement = cnx.prepareStatement("UPDATE category SET libelle = ? WHERE id = ?");
            preparedStatement.setString(1, "Mon nouveau libelle");
            preparedStatement.setInt(2, 4);
            preparedStatement.executeUpdate();
            cnx.close();
        } catch (SQLException e) {
            System.out.println("Une erreur est survenue lors de la connexion à la base de données");
            e.printStackTrace();
        }
    }
}
