package com.humanbooster;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;

public class Delete {
    public static void main(String[] args) {
        String url="jdbc:mysql://localhost/jdbccourses";
        String user="root";
        String password="tiger";
        try {
            Connection cnx = DriverManager.getConnection(url,
                    user, password);
            PreparedStatement preparedStatement = cnx.prepareStatement("DELETE FROM category WHERE id = ?");
            preparedStatement.setInt(1, 4);
            preparedStatement.execute();
            cnx.close();
        } catch (SQLException e) {
            System.out.println("Une erreur est survenue lors de la connexion à la base de données");
            e.printStackTrace();
        }
    }

}
